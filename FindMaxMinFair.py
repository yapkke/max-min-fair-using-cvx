#!/usr/bin/env python
import sys
from cvxopt import solvers, matrix, spdiag, log, printing
printing.options['width'] = -1

Capacity = matrix([10,10],(1,2))
Demand = matrix([10,2,10],(1,3))
Mapping = matrix([0,1,1,
                  1,1,0],(2,3))

#Capacity = matrix([10,30],(1,2))
#Demand = matrix([5,100,100],(1,3))
#Mapping = matrix([1,0,0,
#                  1,1,1],(2,3))

#Capacity = matrix([30,10],(1,2))
#Demand = matrix([100,100,100],(1,3))
#Mapping = matrix([1,1,1,
#                  1,1,0],(2,3))

#Capacity = matrix([30,10],(1,2))
#Demand = matrix([100,100,100],(1,3))
#Mapping = matrix([1,1,1,
#                  1,1,1],(2,3))

#Capacity = matrix([10,10],(1,2))
#Demand = matrix([5,5,5],(1,3))
#Mapping = matrix([1,1,1,
#                  1,1,1],(2,3))

print "Capacity"
print Capacity
print "Demand"
print Demand
print "Mapping"
print Mapping

smallv = 0.0001

m,n = Mapping.size
length = m*n
G = matrix([], (0,length))
h = matrix([], (0,1))

##Positive constraints
for i in range(0,m):
    for j in range(0,n):
        a = matrix(0.0, (1,length))
        a[0,(j*m)+i] = -1.0

        h = matrix([h,0.0])
        G = matrix([G,a])


##Routing constraints
for i in range(0,m):
    for j in range(0,n):
        if (Mapping[i,j] == 0):
            a = matrix(0.0, (1,length))
            a[0,(j*m)+i] = 1.0

            h = matrix([h,smallv])
            G = matrix([G,a])

##Demand constraints
for j in range(0,n):
    g = matrix(0.0, (1,length))

    for i in range(0,m):
        g[0,(j*m)+i] = 1.0

    h = matrix([h,float(Demand[0,j])])
    G = matrix([G,g])

##Capacity constraints
for i in range(0,m):
    g = matrix(0.0, (1,length))

    for j in range(0,n):
        g[0,(j*m)+i] = 1.0

    h = matrix([h,float(Capacity[0,i])])
    G = matrix([G,g])

print "G"
print G
print "h"
print h

def F(x=None, z=None):
    if x is None:
        start = smallv*matrix(1.0,(length,1))
        return 0, start

    f = 0
    for j in range(0,n):
        fterm = 0
        for i in range(0,m):
            fterm = fterm + x[(j*m)+i]
        f = f - log(fterm)

    Df = matrix(0.0,(1, length))
    for j in range(0,n):
        fterm = 0
        for i in range(0,m):
            fterm = fterm + x[(j*m)+i]
        for i in range(0,m):
            Df[j*m+i] = -(1.0/fterm)
    if z is None: return f, Df

    H = matrix(0.0, (length, length))
    for j in range(0,n):
        fterm = 0
        for i in range(0,m):
            fterm = fterm + x[(j*m)+i]
            
        for i in range(0,m):
            for k in range(0,m):
                H[j*m+i, j*m+k] =  fterm**-2.0

    return f, Df, H

solution = solvers.cp(F, G=G, h=h)['x']

print
print "Allocation"
print solution

print "Demand"
print Demand
fa = []
for j in range(0,n):
    s = 0
    for i in range(0,m):
        s = s+ solution[(j*m)+i]
    fa.append(s)
print fa

print
print "Capacity"
print Capacity
fa = []
for i in range(0,m):
    s = 0
    for j in range(0,n):
        s = s+ solution[(j*m)+i]
    fa.append(s)
print fa
